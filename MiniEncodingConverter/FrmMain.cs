﻿using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ConvertToANSI
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_DragDrop(object sender, DragEventArgs e)
        {
            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
            foreach (string file in files) ConvertFileAndWrite(file);
        }

        private void FrmMain_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop)) e.Effect = DragDropEffects.Copy;
            else e.Effect = DragDropEffects.None;
        }

        private void FrmMain_DragLeave(object sender, EventArgs e)
        {

        }

        private void FrmMain_DragOver(object sender, DragEventArgs e)
        {

        }

        private void ConvertFileAndWrite(string filePath)
        {
            new Thread(() =>
            {
                const string txtFormat = ".csv";
                FileInfo fileInfo = new FileInfo(filePath);

                if (!fileInfo.Extension.Equals(txtFormat))
                    return;

                if (File.Exists(filePath))
                {
                    StringBuilder builder = new StringBuilder(File.ReadAllText(filePath));
                    builder.Insert(0, "sep=," + Environment.NewLine);
                    builder.Replace('.', '\'');

                    int name = 1;
                    string newName = fileInfo.Name.Split('.')[0] + " (" + name + ")";
                    string fullNewPath = fileInfo.DirectoryName + "\\" + newName + fileInfo.Extension;

                    while (File.Exists(fullNewPath))
                    {
                        name++;
                        newName = fileInfo.Name.Split('.')[0] + " (" + name + ")";
                        fullNewPath = fileInfo.DirectoryName + "\\" + newName + fileInfo.Extension;
                    }

                    File.WriteAllText(fullNewPath, builder.ToString(), Encoding.GetEncoding("windows-1251"));
                    Done();
                }

            }).Start();
        }

        private void Done()
        {
            Invoke((MethodInvoker)delegate()
            {
                MessageBox.Show("Готово! Файл в той же папке.", "Успех", MessageBoxButtons.OK, MessageBoxIcon.Information);
            });
        }
    }
}
